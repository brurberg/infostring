package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os/exec"
	"strconv"
	"sync"
	"time"

	"gitlab.com/brurberg/gmailstatcli/util"
)

func getCpuAvgUsage() float64 {
	out, err := exec.Command("sh", "cpuAvgUtil").Output()
	if err != nil {
		log.Println(err)
		return -1
	}
	f, err := strconv.ParseFloat(string(out[:len(out)-1]), 64)
	if err != nil {
		log.Println(err)
		return -1
	}
	return f
}

func getCpuTemp() float64 {
	out, err := exec.Command("sh", "cpuTempUtil").Output()
	if err != nil {
		log.Println(err)
		return -1
	}
	f, err := strconv.ParseFloat(string(out[1:len(out)-4]), 64)
	if err != nil {
		log.Println(err)
		return -1
	}
	return f
}

func getBat() float64 {
	max, err := ioutil.ReadFile("/sys/class/power_supply/BAT0/energy_full")
	if err != nil {
		log.Println(err)
		return -1
	}
	now, err := ioutil.ReadFile("/sys/class/power_supply/BAT0/energy_now")
	if err != nil {
		log.Println(err)
		return -1
	}
	maxF, err := strconv.ParseFloat(string(max[:len(max)-1]), 64)
	if err != nil {
		log.Println(err)
		return -1
	}
	nowF, err := strconv.ParseFloat(string(now[:len(now)-1]), 64)
	if err != nil {
		log.Println(err)
		return -1
	}
	if nowF == 0 || maxF == 0 {
		return -1
	}
	return nowF / maxF * 100
}

func gettingBySecond(bySecond chan<- string, wg *sync.WaitGroup) {
	defer wg.Done()
	for {
		bySecond <- fmt.Sprintf("%s %4.1f°C %4.1f%%", time.Now().Format(time.RFC1123), getCpuTemp(), getCpuAvgUsage())
		time.Sleep(1 * time.Second)
	}
}

func gettingAntMail(antMail chan<- string, wg *sync.WaitGroup) {
	defer wg.Done()
	for {
		am, err := util.GetAntMail()
		if err != nil {
			log.Println(err)
		}
		antMail <- am
		time.Sleep(15 * time.Second)
	}
}

func gettingBat(bat chan<- string, wg *sync.WaitGroup) {
	defer wg.Done()
	for {
		bat <- fmt.Sprintf("%4.1f%%", getBat())
		time.Sleep(90 * time.Second)
	}
}

func main() {
	wg := sync.WaitGroup{}
	wg.Add(1)
	antMail := make(chan string)
	go gettingAntMail(antMail, &wg)
	wg.Add(1)
	bySecond := make(chan string)
	go gettingBySecond(bySecond, &wg)
	wg.Add(1)
	bat := make(chan string)
	go gettingBat(bat, &wg)
	bs := ""
	bt := ""
	am := ""
	for {
		select {
		case bt = <-bat:
		case am = <-antMail:
		case bs = <-bySecond: //<-time.After(1 * time.Second):
		}
		exec.Command("xsetroot", "-name", fmt.Sprintf("%s✉ %s %s", am, bt, bs)).Run()
	}
	wg.Wait()
}
